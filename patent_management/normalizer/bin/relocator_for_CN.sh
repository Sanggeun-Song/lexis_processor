#!/bin/bash

#MYSQL_LOGIN_PATH="PATENT_MANAGEMENT_DB"
MYSQL_LOGIN_PATH="PMDB"
MYSQL_DATABASE_NAME="PATENT_MANAGEMENT_DB"
LEXIS_DATA_TABLE="SIPO_SOURCE_FILE_MANAGEMENT"
MYSQL_DEFAULT_TABLE=${LEXIS_DATA_TABLE}

SIPO_FILE_TYPES=("XML" "IMAGES" "PDF")

SIPO_DST_DIR="/patent/sipo"
SIPO_FILE_SERVERS_DIR=("/fileservers/sipo")
#SIPO_XML_STORAGE="/patent/deutschland/source/xml_storage"

function get_data_from_db() {
	local cond=$1
	local colm=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

    local query="select $colm from $table_name where $cond"
    local result=""
    IFS=$'\n' result=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
    if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -1
	fi

	echo $result
}

function upsert_data_to_db () {
	local col_names=$1
	local set_values=$2
	local update_values=$3
	local table_name=$4
	if [ ${#col_names} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
		exit -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	local query="INSERT INTO ${table_name} ($col_names) VALUES (${set_values}) ON DUPLICATE KEY UPDATE $3;"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -1
	fi
}

function get_upsert_query () {
	local col_names=$1
	local set_values=$2
	local update_values=$3
	local table_name=$4
	if [ ${#col_names} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
		exit -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	echo "INSERT INTO ${table_name} ($col_names) VALUES (${set_values}) ON DUPLICATE KEY UPDATE $3;"
}

function update_data_to_db () {
	local conds=$1
	local set_values=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

	if [ ${#conds} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
			end_of_process -1
	fi

	local query="update ${table_name} set ${set_values}  where ${conds}"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -11
	fi
}




function getPublicationDate() {
	local src_xml_path=$1
	local content=`head -n 30 $src_xml_path | tr -d '\r\n\t' | tr '[A-Z]' '[a-z]'`
	if [[ $content =~ \<publication-reference.+?\<date\>([0-9]{8})\<\/date\>.+?\<\/publication-reference\> ]]; then
		echo "${BASH_REMATCH[1]}"
		return 0
	fi
	
	echo -e "NULL"
	return 1
}

function getModifiedFileDate() {
	local src_xml_path=$1
	local content=`head -n 5 $src_xml_path | tr '[A-Z]' '[a-z]' | grep 'lexisnexis-patent-document' | tr -d '\r\n\t'`
	if [[ $content =~ date-changed=\"([^\"]+?)\" ]]; then
		echo "${BASH_REMATCH[1]}"
		return 0
	fi

	echo -e "NULL"
	return 1
}


function getPDFInfo() {
	local src_xml_path=$1
	local content=`tail -n 100 $src_xml_path | grep '<image' | tr -d '\r\n\t'`
	
	local pdf_file_name="NULL"
	local pdf_file_size="NULL"
	local pdf_num_pages="NULL"

	if [[ $content =~ file=\"([^\"]+?)\" ]]; then
		pdf_file_name=${BASH_REMATCH[1]}
	fi
	if [[ $content =~ size=\"([^\"]+?)\" ]]; then		
		pdf_file_size=${BASH_REMATCH[1]}
	fi
	if [[ $content =~ pages=\"([^\"]+?)\" ]]; then		
		pdf_num_pages=${BASH_REMATCH[1]}
	fi

	echo -e "$pdf_file_name\t$pdf_file_size\t$pdf_num_pages"
}

function getImagesInfo() {
	local src_xml_path=$1
	local content=`cat $src_xml_path | tr '\r\n' '\t'`

	if [[ $content =~ \<drawings.+\<\/drawings\> ]]; then
		local img_section=(`echo ${BASH_REMATCH#*>} | sed "s/\<\/drawings\>//g" | sed "s/\ /\n/g" | grep -i "file=" | grep -i "\.png"`)	
		local num_of_imgs=${#img_section[@]}
		local img_list=`echo ${img_section[@]} | sed "s/file=\"//g" | tr '"' ';' | tr -d ' '`
		echo -e "$num_of_imgs\t${img_list%;}"
	else 
		echo -e "NULL\tNULL"
	fi
}

function backup_xmls() {
	local src_path=$1
	local publ_number=$2
	local dst_file_name=$3
	
	local req_date_dir=""
	if [[ $src_path =~ \/([0-9]{4}\.?[0-9]{2}\.?[0-9]{2})\/ ]]; then
		local req_date=`echo ${BASH_REMATCH[1]} | tr -d '.'`
		req_date_dir=${req_date:0:4}/${req_date}/
	else
		local date=`date '+%Y%m%d'`
		req_date_dir=${date:0:4}/${date}/
	fi

	local dst_storage_dir="$SIPO_XML_STORAGE/$req_date_dir${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}"
	if ! [ -d $dst_storage_dir ]; then
#		echo $dst_storage_dir
		mkdir -p $dst_storage_dir
	fi

#	echo "cp $src_path $dst_storage_dir/$dst_file_name"
	cp $src_path $dst_storage_dir/$dst_file_name
	if [ $? == "0" ]; then
		echo -e "\tBACKUP_IS_SUCCESS\t$src_path\t$dst_storage_dir/$dst_file_name"
	else 
		echo -e "\tBACKUP_IS_FAILURE\t$src_path\t$dst_storage_dir/$dst_file_name"
		exit -1
	fi
}

function relocate_file_xml() {
	local src_search_dir=$1
	local update_date=`echo $2 | tr -d '.'`
#	local pub_date=$2
	local src_path=""

	local insert_column_list="DOCUMENT_ID, LEXIS_ID, PUBLICATION_DATE, MODIFIED_DATE, XML_FILE_NAME, ORIGIN_XML_FILE_NAME, PDF_FILE_NAME_FROM_XML, PDF_FILE_SIZE_FROM_XML, PDF_NUM_OF_PAGES_FROM_XML, NUMBER_OF_IMAGES_FROM_XML, IMAGE_FILES_FROM_XML, STORAGE_DIR, XML_UPDATE_DATE, XML_STATUS"
	local upsert_param="MODIFIED_DATE=VALUES(MODIFIED_DATE), LEXIS_ID=VALUES(LEXIS_ID), PUBLICATION_DATE=VALUES(PUBLICATION_DATE), XML_FILE_NAME=VALUES(XML_FILE_NAME), ORIGIN_XML_FILE_NAME=VALUES(ORIGIN_XML_FILE_NAME), NUMBER_OF_IMAGES_FROM_XML=VALUES(NUMBER_OF_IMAGES_FROM_XML), IMAGE_FILES_FROM_XML=VALUES(IMAGE_FILES_FROM_XML), PDF_FILE_NAME_FROM_XML=VALUES(PDF_FILE_NAME_FROM_XML), PDF_NUM_OF_PAGES_FROM_XML=VALUES(PDF_NUM_OF_PAGES_FROM_XML), PDF_FILE_SIZE_FROM_XML = VALUES(PDF_FILE_SIZE_FROM_XML), STORAGE_DIR=VALUES(STORAGE_DIR), XML_UPDATE_DATE=VALUES(XML_UPDATE_DATE), XML_STATUS=VALUES(XML_STATUS)"


	echo -e "RELOCATE XML IS STARTED:\t$src_dir"

	local path_dir=$SRC_DIR
	if ! [ -f $path_dir/${update_date}_xml.path ]; then
		echo -e "\tMAKE_FILE_PATH\t$path_dir/${update_date}.path"
		find $src_search_dir -type f -iname "*.xml" | sort -r > $path_dir/${update_date}_xml.path
	fi

	local max_work=`cat $path_dir/${update_date}_xml.path | wc -l`
	local seq_number=0

	while read src_path
	do
		seq_number=`expr $seq_number + 1`
		if ! [ -f $src_path ]; then
#			echo -e "\t($seq_number/$max_work)\tPASSED\tFILE_NOT_FOUND\t$src_path"
			continue
		fi

		local file_name=${src_path##*/}
		if ! [[ $file_name =~ [C|c][N|n]([0-9]+)([A-Z|a-z][0-9]?)\.([a-z|A-Z]{3}) ]]; then
			echo -e "\t($seq_number/$max_work)\tERROR\tPATTERN_NOT_MATCHED\t$src_path"
			exit -1
		fi
		
		local patent_number=`expr ${BASH_REMATCH[1]} + 0`
		local doc_type=${BASH_REMATCH[2],,}
		local publ_number=`printf "%012d" $patent_number`

		local dst_dir="$SIPO_DST_DIR/${publ_number:0:4}/${publ_number:4:4}/${publ_number}$doc_type"
		if ! [ -d $dst_dir ]; then
			mkdir -p $dst_dir
#			echo "mkdir -p $dst_dir"
		fi
		
		local lexis_doc_id=${file_name%.*}
		local our_doc_id="cn${publ_number}$doc_type"
		local xml_modified_date=$(getModifiedFileDate $src_path)
		local dst_file_name=${our_doc_id}.xml

		local dst_file_path=$dst_dir/$dst_file_name
		if [ -f $dst_file_path ]; then
			local dst_xml_modified_date=$(getModifiedFileDate $dst_file_path)
			if [ $dst_xml_modified_date -ge $xml_modified_date ]; then
				echo -e "\t($seq_number/$max_work)\tPASSED\tThis files is equal or older then storaged file($our_doc_id, $dst_file_path, $dst_xml_modified_date >= $xml_modified_date)"
#				echo "rm -f $src_path"
#				rm -f $src_path
				continue
			fi
		fi

		local xml_pub_date=$(getPublicationDate $src_path)
		local pdf_info=$(getPDFInfo $src_path)
		local img_info=$(getImagesInfo $src_path)
		
		local params
		IFS=$'\t' params=(`echo -e "$xml_pub_date\t$xml_modified_date\t$pdf_info\t$img_info"`)	
#		echo "cp -n $src_path $dst_file_path"	
		cp $src_path $dst_file_path
#		mv $src_path $dst_file_path
		if [ $? == "0" ]; then
			local insert_params=`echo "'$our_doc_id','$lexis_doc_id', '${params[0]}', '${params[1]}', '${dst_file_name}', '${file_name}', '${params[2]}', ${params[3]}, ${params[        4]}, ${params[5]}, '${params[6]}', '$dst_dir', '$update_date', 'SUCCESS'" | sed "s/\'NULL\'/NULL/g"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\t($seq_number/$max_work)\t\tNORMALIZATION_IS_SUCCESS\t$our_doc_id\t$dst_file_path"
			IFS=$'\n'
			continue
		else 
			local insert_params=`echo "'$our_doc_id','$lexis_doc_id', '${params[0]}', '${params[1]}', '${dst_file_name}', '${file_name}', '${params[2]}', ${params[3]}, ${params[        4]}, ${params[5]}, '${params[6]}', '$dst_dir', '$update_date', 'FAILURE'" | sed "s/\'NULL\'/NULL/g"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\t($seq_number/$max_work)\t\tNORMALIZATION_IS_FAILURE\t$our_doc_id\t$dst_file_path"
			IFS=$'\n'
			exit -1
		fi
	done < $path_dir/${update_date}_xml.path

	echo -e "\nRELOCATE XML IS FINISHED:\t$src_dir\n\n"
}


function convert_png_to_resized_img() {
	local src_file_path=$1
	local dst_dir=$2
	local src_file_name=${src_file_path##*/}
	local postfix
	if ! [ -f $src_file_path ]; then
		return 0
	fi

	result=0
	for postfix in `echo '600' '360' '180'`
	do
		local option
		local dst_file_name
		case $postfix in
			'180')
		        option="-density 300 -units PixelsPerInch -resize 180x180"
				dst_file_name="${src_file_name%.*}_s.jpg"
				;;
			'360')
		        option="-density 300 -units PixelsPerInch -resize 360x360"
				dst_file_name="${src_file_name%.*}_m.jpg"
				;;
			'600')
		        option="-density 300 -units PixelsPerInch -resize 600x600"
				dst_file_name="${src_file_name%.*}_l.jpg"
				;;	
		esac

		if ! [ -d $dst_dir ]; then
#			echo "mkdir -p $dst_dir"
			mkdir -p $dst_dir
		elif [ -f $dst_dir/$dst_file_name ]; then
			result=`expr $result + 1`
			continue
		fi

		convert $option $src_file_path $dst_dir/$dst_file_name
		if [ $? == "0" ]; then
			result=`expr $result + 1`
		fi
	done
	
	return $result
}


function image_transfer_to_fileserver() {
	local src_file_path=$1
	local converted_images_dir=$2
	local relative_dir=$3
	
	local result=0
	local file_server
	for file_server in `echo ${SIPO_FILE_SERVERS_DIR[@]}`
	do
		echo $file_server
		if [ -d $file_server ]; then
			local file_server_dir=$file_server/$relative_dir
			if ! [ -d $file_server_dir ]; then
				echo "mkdir -p $file_server_dir_dir"
#				mkdir -p $file_server_dir
			fi

#			local file_server_path=$file_server_dir/$src_file_path
			echo "cp -n $src_file_path $file_server_dir/"
#			cp -n $src_file_path $file_server_dir/
			if [ $? != "0" ]; then
				result=`expr $result + 1`
			fi
		fi
	done 

	return $result
}


function pdf_transfer_to_fileserver() {
	local src_file_path=$1
	local relative_dir=$2
	local dst_file_name=$3	
	local result=0
	local file_server
	for file_server in `echo ${SIPO_FILE_SERVERS_DIR[@]}`
	do
		if [ -d $file_server ]; then
			local file_server_dir=$file_server/$relative_dir
			if ! [ -d $file_server_dir ]; then
#				echo "mkdir -p $file_server_dir"
				mkdir -p $file_server_dir
			fi

#			echo "cp -n $src_file_path $file_server_dir/$dst_file_name"
			cp -n $src_file_path $file_server_dir/$dst_file_name
			if [ $? != "0" ]; then
				result=`expr $result + 1`
			fi
		fi
	done 

	return $result
}


function relocate_file_image() {
	local src_search_dir=$1
	local update_date=`echo $2 | tr -d '.'`
	local src_path=""

	echo -e "RELOCATE IMAGE IS STARTED:\t$src_dir"
			
	local insert_column_list="DOCUMENT_ID, LEXIS_ID, ORIGIN_NUMBER_OF_IMAGES, ORIGIN_IMAGE_FILES, STORAGE_DIR, IMAGE_UPDATE_DATE"
#	local upsert_param="SRC_NUMBER_OF_IMAGES=VALUES(SRC_NUMBER_OF_IMAGES), ORIGIN_IMAGE_FILESS=VALUES(ORIGIN_IMAGE_FILESS), IMAGE_STATUS='REQ_VALID', IMAGE_FILES=VALUES(ORIGIN_IMAGE_FILESS)"
	
	for src_path in `find $src_search_dir -type f -iname "*.xml" | sort -r`
	do
		local upsert_param="LEXIS_ID=VALUES(LEXIS_ID), ORIGIN_NUMBER_OF_IMAGES=VALUES(ORIGIN_NUMBER_OF_IMAGES), ORIGIN_IMAGE_FILES=VALUES(ORIGIN_IMAGE_FILES), IMAGE_UPDATE_DATE=VALUES(IMAGE_UPDATE_DATE), STORAGE_DIR=VALUES(STORAGE_DIR)"
		local src_dir=${src_path%/*}
		local file_name=${src_path##*/}
		file_name=${file_name%.*}
		if ! [[ $file_name =~ [C|c][n|N]([0-9]+)([A-Z|a-z][0-9]?) ]]; then
			echo -e "\tERROR\tPATTERN_NOT_MATCHED\t$src_path"
			exit -1
		fi
		
#		local image_info=$(getImagesInfo "$src_path")
		local image_info=(`echo -e "$(getImagesInfo "$src_path")" | tr '\t' '\n' `)
	
#		echo -e "${#image_info[@]}\t$xml_num_of_images\t$xml_image_lists"
		
		local xml_num_of_images=${image_info[0]}
		local xml_image_lists=${image_info[1]}
		
		local patent_number=`expr ${BASH_REMATCH[1]} + 0`
		local doc_type=${BASH_REMATCH[2],,}
		local publ_number=`printf "%012d" $patent_number`
		
		local lexis_doc_id=${file_name%.*}
		local our_doc_id="cn$publ_number$doc_type"
		
		local dst_dir="$SIPO_DST_DIR/${publ_number:0:4}/${publ_number:4:4}/${publ_number}$doc_type"
		
		local insert_params=`echo "'$our_doc_id','$lexis_doc_id', ${xml_num_of_images}, '${xml_image_lists}', '$dst_dir', '$update_date'" | sed "s/\'NULL\'/NULL/g"`

#		upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
		if [ $xml_num_of_images == NULL ] || [ $xml_num_of_images -le 0 ]; then
			echo -e "ERROR\t도면이 있다면서 실제로 없는 상황은 무슨 시츄?\n\t$our_doc_id\t$src_dir/${file_name}_*.*\n\t$src_path"
#			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='ERROR_1', ETC='도면이 있다면서 실제로 없는 상황'"
			upsert_param=`echo "$upsert_param, FS_IMAGE_STATUS='ERROR_1', ETC='도면이 있다면서 실제로 없는 상황'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			continue
		fi

		local src_img_path="$src_dir/${file_name}_*.*"
		local real_num_of_images=`ls -al $src_img_path | wc -l`
		if [ $real_num_of_images -ne $xml_num_of_images ]; then
			echo -e "ERROR\t도면 파일 개수가  xml내의 내용과 매칭 되지 않음\n\t$our_doc_id\t$src_dir/${file_name}_*.*\n\t$src_path"
#			update_data_to_db "DOCUMENT_ID='$our_doc_id'" 
			"FS_IMAGE_STATUS='ERROR_2', ETC='도면 파일 개수가  xml내의 내용과 매칭 되지 않음'"
			upsert_param=`echo "$upsert_param, FS_IMAGE_STATUS='ERROR_2', ETC='도면 파일 개수가  xml내의 내용과 매칭 되지 않음'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			continue
		fi

		# 파일서버 먼저 보내자...
		local file_server_dir="${SIPO_FILE_SERVERS_DIR[0]}/${publ_number:0:4}/${publ_number:4:4}/${publ_number}/$doc_type/img"
		if ! [ -d $file_server_dir ]; then
			mkdir -p $file_server_dir
#			echo "mkdir -p $file_server_dir"
		fi

#		echo "cp $src_img_path $file_server_dir/"
		cp $src_img_path $file_server_dir
		if [ $? == "0" ]; then
#			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='SUCCESS'"
			upsert_param=`echo "$upsert_param, FS_IMAGE_STATUS='SUCCESS'"`
			echo -e "\tSUCCESS_TO_WRITE_FILE_SERVER\t$our_doc_id\t$file_server_dir/"
		else 
#			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='FAILURE'"
			upsert_param=`echo "$upsert_param, FS_IMAGE_STATUS='FAILURE'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\tFAILED_TO_WRITE_FILE_SERVER\t$our_doc_id\t$file_server_dir/"
			exit -1
		fi

		local dst_dir="$SIPO_DST_DIR/${publ_number:0:4}/${publ_number:4:4}/${publ_number}$doc_type"
		if ! [ -d $dst_dir ]; then
#			echo $dst_dir
			mkdir -p $dst_dir
		fi
		
#		echo "cp $src_img_path $dst_dir"
#		mv $src_img_path $dst_dir/
		cp $src_img_path $dst_dir/
		if [ $? == "0" ]; then
#			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "IMAGE_STATUS='SUCCESS'"
			upsert_param=`echo "$upsert_param, IMAGE_STATUS='SUCCESS'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\tNORMALIZATION_IS_SUCCESS\t$our_doc_id\t$dst_dir"
#			rm -f $src_path
#			echo "rm -f $src_path"
		else 
#			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "IMAGE_STATUS='FAILURE'"
			upsert_param=`echo "$upsert_param, IMAGE_STATUS='SUCCESS'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\tNORMALIZATION_IS_FAILURE\t$our_doc_id\t$dst_dir"
			exit -1
		fi
	done

	echo -e "\nRELOCATE IMAGE IS FINISHED:\t$src_dir\n\n"
}

function relocate_file_pdf() {
	local src_search_dir=$1
	local update_date=`echo $2 | tr -d '.'`
	local src_path=""

	echo -e "RELOCATE PDF IS STARTED:\t$src_dir"
	
	local insert_column_list="DOCUMENT_ID, LEXIS_ID, PDF_FILE_NAME, ORIGIN_PDF_FILE_NAME, ORIGIN_PDF_FILE_SIZE, STORAGE_DIR, PDF_UPDATE_DATE, FS_PDF_STATUS, PDF_STATUS"
	local upsert_param="LEXIS_ID=VALUES(LEXIS_ID), PDF_FILE_NAME=VALUES(PDF_FILE_NAME), ORIGIN_PDF_FILE_NAME=VALUES(ORIGIN_PDF_FILE_NAME), ORIGIN_PDF_FILE_SIZE=VALUES(ORIGIN_PDF_FILE_SIZE), PDF_UPDATE_DATE=VALUES(PDF_UPDATE_DATE), STORAGE_DIR=VALUES(STORAGE_DIR), FS_PDF_STATUS=VALUES(FS_PDF_STATUS), PDF_STATUS=VALUES(PDF_STATUS)"
	
	for src_path in `find $src_search_dir -type f -iname "*.pdf" | sort -r`
	do
		local src_dir=${src_path%/*}
		local file_name=${src_path##*/}
		if ! [[ $file_name =~ [C|c][n|N]([0-9]+)([A-Z|a-z][0-9]?) ]]; then
			echo -e "\tERROR\tPATTERN_NOT_MATCHED\t$src_path"
			exit -1
		fi
		
		local patent_number=`expr ${BASH_REMATCH[1]} + 0`
		local doc_type=${BASH_REMATCH[2],,}
		local publ_number=`printf "%012d" $patent_number`
		
		local lexis_doc_id=${file_name%.*}
		local our_doc_id="cn$publ_number$doc_type"
		
		local dst_dir="$SIPO_DST_DIR/${publ_number:0:4}/${publ_number:4:4}/${publ_number}$doc_type"
		

		local dst_file_name=${our_doc_id}.pdf
		local file_size=`ls -al $src_path | cut -d ' ' -f 5`

		local insert_params=`echo "'$our_doc_id','$lexis_doc_id', '${dst_file_name}', '${file_name}', $file_size, '$dst_dir', '$update_date'" | sed "s/\'NULL\'/NULL/g"`		
#		echo $insert_params	
		if ! [ -d $dst_dir ]; then
			mkdir -p $dst_dir
#			echo "mkdir -p $dst_dir"
		fi
		local additional_params=""
			
		#파일서버로 보내자...
		local relative_dir="${publ_number:0:4}/${publ_number:4:4}/${publ_number}/$doc_type/pdf"
		pdf_transfer_to_fileserver "$src_path" "$relative_dir" "$dst_file_name"
		if [ $? == "0" ]; then
			insert_params=`echo -e "$insert_params, 'SUCCESS'"`
			echo -e "\tSUCCESS_TO_WRITE_FILE_SERVER\t$our_doc_id\t$relative_dir/"
		else 
			insert_params=`echo -e "$insert_params, 'FAILURE'"`
			echo -e "\tFAILED_TO_WRITE_FILE_SERVER\t$our_doc_id\t$relative_dir/$dst_file_name"
		fi
			
		local dst_file_path="$dst_dir/$dst_file_name"
#		echo "cp $src_path $dst_file_path"
#		mv $src_path $dst_dir/$dst_file_name
		cp -n $src_path $dst_dir/$dst_file_name
		if [ $? == "0" ]; then
			insert_params=`echo -e "$insert_params, 'SUCCESS'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\tNORMALIZATION_IS_SUCCESS\t$our_doc_id\t$dst_file_path"
		else 
			insert_params=`echo -e "$insert_params, 'FAILURE'"`
			upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
			echo -e "\tNORMALIZATION_IS_FAILURE\t$our_doc_id\t$dst_file_path"
			exit -1
		fi
		
	done

	echo -e "\nRELOCATE PDF IS FINISHED:\t$src_dir\n\n"
}


function relocate_file() {
	local src_dir=$1
	local file_type=$2 
	local pub_date=$3
	local dst_dir=""

	case $file_type in
		"XML")
			relocate_file_xml "$src_dir" "$pub_date"
			;;
		"IMAGES")
			relocate_file_image "$src_dir" "$pub_date"
			;;
		"PDF")
			relocate_file_pdf "$src_dir" "$pub_date"
			;;
	esac

}

function main() {
	local pub_date=$1
	local SRC_DIR=$2
	if ! [ -z $3 ]; then
		SIPO_DST_DIR=$3
	fi
	local file_type=""
	local i=0
	for ((i=0; i<${#SIPO_FILE_TYPES}; i++))
	do
		local src_dir=$SRC_DIR/${SIPO_FILE_TYPES[i]}
		if [ -d $src_dir ]; then
			echo "relocate_file $src_dir ${SIPO_FILE_TYPES[i]} $pub_date"
			relocate_file $src_dir ${SIPO_FILE_TYPES[i]} $pub_date
		fi
	done

	exit 0
}

main $1 $2 $3
