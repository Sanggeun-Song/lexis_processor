#!/bin/bash

#MYSQL_LOGIN_PATH="PATENT_MANAGEMENT_DB"
MYSQL_LOGIN_PATH="PMDB"
MYSQL_DATABASE_NAME="PATENT_MANAGEMENT_DB"
LEXIS_DATA_TABLE="DPMA_SOURCE_FILE_MANAGEMENT"
MYSQL_DEFAULT_TABLE=${LEXIS_DATA_TABLE}


SIPO_FILE_TYPES=("XML" "IMAGES" "PDF")

DPMA_DST_DIR="/patent/deutschland/normalized/dpma"

DPMA_FILE_SERVER_DIR="/fileservers/dpma"

DPMA_XML_STORAGE="/patent/deutschland/source/xml_storage"

function get_data_from_db() {
	local cond=$1
	local colm=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

    local query="select $colm from $table_name where $cond"
    local result=""
    IFS=$'\n' result=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
    if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -1
	fi

	echo $result
}

function upsert_data_to_db () {
	local col_names=$1
	local set_values=$2
	local update_values=$3
	local table_name=$4
	if [ ${#col_names} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
		exit -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	local query="INSERT INTO ${table_name} ($col_names) VALUES (${set_values}) ON DUPLICATE KEY UPDATE $3;"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -1
	fi
}

function update_data_to_db () {
	local conds=$1
	local set_values=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

	if [ ${#conds} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
			end_of_process -1
	fi

	local query="update ${table_name} set ${set_values}  where ${conds}"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -11
	fi
}




function getPublicationDate() {
	local src_xml_path=$1
	local content=`head -n 30 $src_xml_path | tr -d '\r\n\t' | tr '[A-Z]' '[a-z]'`
	if [[ $content =~ \<publication-reference.+?\<date\>([0-9]{8})\<\/date\>.+?\<\/publication-reference\> ]]; then
		echo "${BASH_REMATCH[1]}"
		return 0
	fi
	
	echo -e "NULL"
	return 1
}

function getModifiedFileDate() {
	local src_xml_path=$1
	local content=`head -n 5 $src_xml_path | tr '[A-Z]' '[a-z]' | grep 'lexisnexis-patent-document' | tr -d '\r\n\t'`
	if [[ $content =~ date-changed=\"([^\"]+?)\" ]]; then
		echo "${BASH_REMATCH[1]}"
		return 0
	fi

	echo -e "NULL"
	return 1
}


function getPDFInfo() {
	local src_xml_path=$1
	local content=`tail -n 100 $src_xml_path | grep '<image' | tr -d '\r\n\t'`
	
	local pdf_file_name="NULL"
	local pdf_file_size="NULL"
	local pdf_num_pages="NULL"

	if [[ $content =~ file=\"([^\"]+?)\" ]]; then
		pdf_file_name=${BASH_REMATCH[1]}
	fi
	if [[ $content =~ size=\"([^\"]+?)\" ]]; then		
		pdf_file_size=${BASH_REMATCH[1]}
	fi
	if [[ $content =~ pages=\"([^\"]+?)\" ]]; then		
		pdf_num_pages=${BASH_REMATCH[1]}
	fi

	echo -e "$pdf_file_name\t$pdf_file_size\t$pdf_num_pages"
}

function getImagesInfo() {
	local src_xml_path=$1
	local content=`cat $src_xml_path | tr '\r\n' '\t'`

	if [[ $content =~ \<drawings.+\<\/drawings\> ]]; then
		local img_section=(`echo ${BASH_REMATCH#*>} | sed "s/\<\/drawings\>//g" | sed "s/\ /\n/g" | grep -i "file=" | grep -i "\.png"`)	
		local num_of_imgs=${#img_section[@]}
		local img_list=`echo ${img_section[@]} | sed "s/file=\"//g" | tr '"' ';' | tr -d ' '`
		echo -e "$num_of_imgs\t${img_list%;}"
	else 
		echo -e "NULL\tNULL"
	fi
}

function backup_xmls() {
	local src_path=$1
	local publ_number=$2
	local dst_file_name=$3
	
	local req_date_dir=""
	if [[ $src_path =~ \/([0-9]{4}\.?[0-9]{2}\.?[0-9]{2})\/ ]]; then
		local req_date=`echo ${BASH_REMATCH[1]} | tr -d '.'`
		req_date_dir=${req_date:0:4}/${req_date}/
	else
		local date=`date '+%Y%m%d'`
		req_date_dir=${date:0:4}/${date}/
	fi

	local dst_storage_dir="$DPMA_XML_STORAGE/$req_date_dir${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}"
	if ! [ -d $dst_storage_dir ]; then
#		echo $dst_storage_dir
		mkdir -p $dst_storage_dir
	fi

#	echo "cp $src_path $dst_storage_dir/$dst_file_name"
	cp $src_path $dst_storage_dir/$dst_file_name
	if [ $? == "0" ]; then
		echo -e "\tBACKUP_IS_SUCCESS\t$src_path\t$dst_storage_dir/$dst_file_name"
	else 
		echo -e "\tBACKUP_IS_FAILURE\t$src_path\t$dst_storage_dir/$dst_file_name"
		exit -1
	fi
}

function relocate_file_xml() {
	local src_search_dir=$1
	local update_date=`echo $2 | tr -d '.'`
#	local pub_date=$2
	local src_path=""

	local insert_column_list="DOCUMENT_ID, LEXIS_ID, PUBLICATION_DATE, MODIFIED_DATE, XML_FILE_NAME, ORIGIN_XML_FILE_NAME, PDF_FILE_NAME_FROM_XML, PDF_FILE_SIZE_FROM_XML, PDF_NUM_OF_PAGES_FROM_XML, NUMBER_OF_IMAGES_FROM_XML, IMAGE_FILES_FROM_XML, STORAGE_DIR, XML_UPDATE_DATE"
	local upsert_param="MODIFIED_DATE=VALUES(MODIFIED_DATE), LEXIS_ID=VALUES(LEXIS_ID), PUBLICATION_DATE=VALUES(PUBLICATION_DATE), XML_FILE_NAME=VALUES(XML_FILE_NAME), ORIGIN_XML_FILE_NAME=VALUES(ORIGIN_XML_FILE_NAME), NUMBER_OF_IMAGES_FROM_XML=VALUES(NUMBER_OF_IMAGES_FROM_XML), IMAGE_FILES_FROM_XML=VALUES(IMAGE_FILES_FROM_XML), PDF_FILE_NAME_FROM_XML=VALUES(PDF_FILE_NAME_FROM_XML), PDF_NUM_OF_PAGES_FROM_XML=VALUES(PDF_NUM_OF_PAGES_FROM_XML), PDF_FILE_SIZE_FROM_XML = VALUES(PDF_FILE_SIZE_FROM_XML), STORAGE_DIR=VALUES(STORAGE_DIR), XML_UPDATE_DATE=VALUES(XML_UPDATE_DATE)"


	echo -e "RELOCATE XML IS STARTED:\t$src_dir"
	for src_path in `find $src_search_dir -type f -iname "*.xml" | sort -r`
	do
		local src_dir=${src_path%/*}
		local file_name=${src_path##*/}
		if ! [[ $file_name =~ [D|d][E|e]([0-9]+)([A-Z|a-z][0-9]?)\.([a-z|A-Z]{3}) ]]; then
			echo -e "\tERROR\tPATTERN_NOT_MATCHED\t$src_path"
			exit -1
		fi
		
		local patent_number=`expr ${BASH_REMATCH[1]} + 0`
		local doc_type=${BASH_REMATCH[2],,}
		local publ_number=`printf "%012d" $patent_number`
		local dst_dir="$DPMA_DST_DIR/${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}/${publ_number}$doc_type"
		
		local lexis_doc_id=${file_name%.*}
		local our_doc_id="de$publ_number$doc_type"
		

		local xml_pub_date=$(getPublicationDate $src_path)
		local xml_modified_date=$(getModifiedFileDate $src_path)
		local pdf_info=$(getPDFInfo $src_path)
		local img_info=$(getImagesInfo $src_path)
		
		local dst_file_name=${our_doc_id}.xml
		
		local params
		IFS=$'\t' params=(`echo -e "$xml_pub_date\t$xml_modified_date\t$pdf_info\t$img_info"`)
		local tmp=$(get_data_from_db "DOCUMENT_ID='$our_doc_id'" "MODIFIED_DATE") 
		if ! [ -z $tmp ] && [ $xml_modified_date != "NULL" ] && [ $tmp -gt $xml_modified_date ]; then
			if [ $tmp == "NULL" ] || [ $tmp -gt $xml_modified_date ]; then
				echo -e "\tNOTICE: This files is older then storaged file($our_doc_id, $src_path, $tmp > $xml_modified_date)"
				IFS=$'\n'
				continue	
			fi
		fi
		local insert_params=`echo "'$our_doc_id','$lexis_doc_id', '${params[0]}', '${params[1]}', '${dst_file_name}', '${file_name}', '${params[2]}', ${params[3]}, ${params[4]}, ${params[5]}, '${params[6]}', '$dst_dir', '$update_date'" | sed "s/\'NULL\'/NULL/g"`
		upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"

		# 팀장님에게 드릴 XML 모음집 만들기
		backup_xmls $src_path $publ_number $dst_file_name		
		if ! [ -d $dst_dir ]; then
#			echo $dst_dir
			mkdir -p $dst_dir
		fi
		
		local dst_file_path="$dst_dir/$dst_file_name"
#		echo "cp $src_path $dst_file_path"
		mv $src_path $dst_dir/$dst_file_name
		if [ $? == "0" ]; then
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "XML_STATUS='SUCCESS'"
			echo -e "\tNORMALIZATION_IS_SUCCESS\t$src_path\t$dst_file_path"
		else 
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "XML_STATUS='FAILURE'"
			echo -e "\tNORMALIZATION_IS_FAILURE\t$src_path\t$dst_file_path"
			IFS=$'\n'
			exit -1
		fi
		IFS=$'\n'
	done

	echo -e "\nRELOCATE XML IS FINISHED:\t$src_dir\n\n"
}

function relocate_file_image() {
	local src_search_dir=$1
	local update_date=`echo $2 | tr -d '.'`
	local src_path=""

	echo -e "RELOCATE IMAGE IS STARTED:\t$src_dir"
			
	local insert_column_list="DOCUMENT_ID, LEXIS_ID, ORIGIN_NUMBER_OF_IMAGES, ORIGIN_IMAGE_FILES, STORAGE_DIR, IMAGE_UPDATE_DATE"
	local upsert_param="LEXIS_ID=VALUES(LEXIS_ID), ORIGIN_NUMBER_OF_IMAGES=VALUES(ORIGIN_NUMBER_OF_IMAGES), ORIGIN_IMAGE_FILES=VALUES(ORIGIN_IMAGE_FILES), IMAGE_STATUS='REQ_VALID', IMAGE_UPDATE_DATE=VALUES(IMAGE_UPDATE_DATE), STORAGE_DIR=VALUES(STORAGE_DIR)"
#	local upsert_param="SRC_NUMBER_OF_IMAGES=VALUES(SRC_NUMBER_OF_IMAGES), ORIGIN_IMAGE_FILESS=VALUES(ORIGIN_IMAGE_FILESS), IMAGE_STATUS='REQ_VALID', IMAGE_FILES=VALUES(ORIGIN_IMAGE_FILESS)"
	
	for src_path in `find $src_search_dir -type f -iname "*.xml" | sort -r`
	do
		local src_dir=${src_path%/*}
		local file_name=${src_path##*/}
		file_name=${file_name%.*}
		if ! [[ $file_name =~ [D|d][E|e]([0-9]+)([A-Z|a-z][0-9]?) ]]; then
			echo -e "\tERROR\tPATTERN_NOT_MATCHED\t$src_path"
			exit -1
		fi
		
#		local image_info=$(getImagesInfo "$src_path")
		local image_info=(`echo -e "$(getImagesInfo "$src_path")" | tr '\t' '\n' `)
	
#		echo -e "${#image_info[@]}\t$xml_num_of_images\t$xml_image_lists"
		
		local xml_num_of_images=${image_info[0]}
		local xml_image_lists=${image_info[1]}
		
		local patent_number=`expr ${BASH_REMATCH[1]} + 0`
		local doc_type=${BASH_REMATCH[2],,}
		local publ_number=`printf "%012d" $patent_number`
		
		local lexis_doc_id=${file_name%.*}
		local our_doc_id="de$publ_number$doc_type"
		
		local dst_dir="$DPMA_DST_DIR/${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}/${publ_number}$doc_type"
		
		local insert_params=`echo "'$our_doc_id','$lexis_doc_id', ${xml_num_of_images}, '${xml_image_lists}', '$dst_dir', '$update_date'" | sed "s/\'NULL\'/NULL/g"`

		upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
		if [ $xml_num_of_images == NULL ] || [ $xml_num_of_images -le 0 ]; then
			echo -e "ERROR\t도면이 있다면서 실제로 없는 상황은 무슨 시츄?\n\t$our_doc_id\t$src_dir/${file_name}_*.*\n\t$src_path"
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='ERROR_1', ETC='도면이 있다면서 실제로 없는 상황'"
			continue
		fi

		local src_img_path="$src_dir/${file_name}_*.*"
		local real_num_of_images=`ls -al $src_img_path | wc -l`
		if [ $real_num_of_images -ne $xml_num_of_images ]; then
			echo -e "ERROR\t도면 파일 개수가  xml내의 내용과 매칭 되지 않음\n\t$our_doc_id\t$src_dir/${file_name}_*.*\n\t$src_path"
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='ERROR_2', ETC='도면 파일 개수가  xml내의 내용과 매칭 되지 않음'"
			continue
		fi

		# 파일서버 먼저 보내자...
		local file_server_dir="$DPMA_FILE_SERVER_DIR/${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}/${publ_number}/$doc_type/img"
		if ! [ -d $file_server_dir ]; then
			mkdir -p $file_server_dir
#			echo "mkdir -p $file_server_dir"
		fi

#		echo "cp $src_img_path $file_server_dir/"
		cp $src_img_path $file_server_dir
		if [ $? == "0" ]; then
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='SUCCESS'"
			echo -e "\tSUCCESS_TO_WRITE_FILE_SERVER\t$our_doc_id\t$file_server_dir/"
		else 
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_IMAGE_STATUS='FAILURE'"
			echo -e "\tFAILED_TO_WRITE_FILE_SERVER\t$our_doc_id\t$file_server_dir/"
			exit -1
		fi

		local dst_dir="$DPMA_DST_DIR/${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}/${publ_number}$doc_type"
		if ! [ -d $dst_dir ]; then
#			echo $dst_dir
			mkdir -p $dst_dir
		fi
		
#		echo "cp $src_img_path $dst_dir"
		mv $src_img_path $dst_dir/
		if [ $? == "0" ]; then
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "IMAGE_STATUS='SUCCESS'"
			echo -e "\tNORMALIZATION_IS_SUCCESS\t$our_doc_id\t$dst_dir"
			rm -f $src_path
#			echo "rm -f $src_path"
		else 
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "IMAGE_STATUS='FAILURE'"
			echo -e "\tNORMALIZATION_IS_FAILURE\t$our_doc_id\t$dst_dir"
			exit -1
		fi
	done

	echo -e "\nRELOCATE IMAGE IS FINISHED:\t$src_dir\n\n"
}

function relocate_file_pdf() {
	local src_search_dir=$1
	local update_date=`echo $2 | tr -d '.'`
	local src_path=""

	echo -e "RELOCATE PDF IS STARTED:\t$src_dir"
	
	local insert_column_list="DOCUMENT_ID, LEXIS_ID, PDF_FILE_NAME, ORIGIN_PDF_FILE_NAME, ORIGIN_PDF_FILE_SIZE, STORAGE_DIR, PDF_UPDATE_DATE"
	local upsert_param="LEXIS_ID=VALUES(LEXIS_ID), PDF_FILE_NAME=VALUES(PDF_FILE_NAME), ORIGIN_PDF_FILE_NAME=VALUES(ORIGIN_PDF_FILE_NAME), ORIGIN_PDF_FILE_SIZE=VALUES(ORIGIN_PDF_FILE_SIZE), PDF_UPDATE_DATE=VALUES(PDF_UPDATE_DATE), STORAGE_DIR=VALUES(STORAGE_DIR)"
	
	for src_path in `find $src_search_dir -type f -iname "*.pdf" | sort -r`
	do
		local src_dir=${src_path%/*}
		local file_name=${src_path##*/}
		if ! [[ $file_name =~ [D|d][E|e]([0-9]+)([A-Z|a-z][0-9]?)\.([a-z|A-Z]{3}) ]]; then
			echo -e "\tERROR\tPATTERN_NOT_MATCHED\t$src_path"
			exit -1
		fi

		local patent_number=`expr ${BASH_REMATCH[1]} + 0`
		local doc_type=${BASH_REMATCH[2],,}
		local publ_number=`printf "%012d" $patent_number`
		
		local lexis_doc_id=${file_name%.*}
		local our_doc_id="de$publ_number$doc_type"
		
		
		local dst_dir="$DPMA_DST_DIR/${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}/${publ_number}$doc_type"
		local dst_file_name=${our_doc_id}.pdf

		local file_size=`ls -al $src_path | cut -d ' ' -f 5`
		
		local insert_params=`echo "'$our_doc_id','$lexis_doc_id', '${dst_file_name}', '${file_name}', $file_size, '$dst_dir', '$update_date'" | sed "s/\'NULL\'/NULL/g"`

#		echo "$insert_column_list" "$insert_params" "$upsert_param"		
		upsert_data_to_db "$insert_column_list" "$insert_params" "$upsert_param"
		
		# 파일서버 먼저 보내자...
		local file_server_dir="$DPMA_FILE_SERVER_DIR/${publ_number:0:2}/${publ_number:2:4}/${publ_number:6:3}/${publ_number}/$doc_type/pdf"
		if ! [ -d $file_server_dir ]; then
			mkdir -p $file_server_dir
#			echo "mkdir -p $file_server_dir"
		fi

#		echo "cp $src_path $file_server_dir/$dst_file_name"
		cp $src_path $file_server_dir/$dst_file_name
		if [ $? == "0" ]; then
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_PDF_STATUS='SUCCESS'"
			echo -e "\tSUCCESS_TO_WRITE_FILE_SERVER\t$our_doc_id\t$file_server_dir/$dst_file_name"
		else 
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "FS_PDF_STATUS='FAILURE'"
			echo -e "\tFAILED_TO_WRITE_FILE_SERVER\t$our_doc_id\t$file_server_dir/$dst_file_name"
			exit -1
		fi

		
		if ! [ -d $dst_dir ]; then
#			echo $dst_dir
			mkdir -p $dst_dir
		fi
		
		local dst_file_path="$dst_dir/$dst_file_name"
#		echo "cp $src_path $dst_file_path"
		mv $src_path $dst_dir/$dst_file_name
		if [ $? == "0" ]; then
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "PDF_STATUS='SUCCESS'"
			echo -e "\tNORMALIZATION_IS_SUCCESS\t$our_doc_id\t$dst_file_path"
		else 
			update_data_to_db "DOCUMENT_ID='$our_doc_id'" "PDF_STATUS='FAILURE'"
			echo -e "\tNORMALIZATION_IS_FAILURE\t$our_doc_id\t$dst_file_path"
			exit -1
		fi
		
	done

	echo -e "\nRELOCATE PDF IS FINISHED:\t$src_dir\n\n"
}


function relocate_file() {
	local src_dir=$1
	local file_type=$2 
	local pub_date=$3
	local dst_dir=""

	case $file_type in
		"XML")
			relocate_file_xml "$src_dir" "$pub_date"
			;;
		"IMAGES")
			relocate_file_image "$src_dir" "$pub_date"
			;;
		"PDF")
			relocate_file_pdf "$src_dir" "$pub_date"
			;;
	esac

}

function main() {
	local pub_date=$1
	local SRC_DIR=$2
	DPMA_DST_DIR=$3
	local file_type=""
	local i=0
	for ((i=0; i<${#SIPO_FILE_TYPES}; i++))
	do
		local src_dir=$SRC_DIR/${SIPO_FILE_TYPES[i]}
#		echo "$src_dir"
		if [ -d $src_dir ]; then
			relocate_file $src_dir ${SIPO_FILE_TYPES[i]} $pub_date
#		else
#			echo -e "ERROR\tDIRECTORY_IS_MISSING($src_dir)"
#			exit -1
		fi
	done

	exit 0
}

main $1 $2 $3
