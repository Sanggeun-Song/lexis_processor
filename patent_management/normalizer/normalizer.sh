#!/bin/bash

SHELL_PATH=${0}

#MYSQL_LOGIN_PATH="PATENT_MANAGEMENT_DB"
MYSQL_LOGIN_PATH="PMDB"
MYSQL_DATABASE_NAME="PATENT_MANAGEMENT_DB"
LEXIS_DATA_TABLE="LEXIS_DATA_DPMA_PATENT_SOURCE"

MYSQL_DEFAULT_TABLE=${LEXIS_DATA_TABLE}

PROGRAM_HOST_NAME=`hostname | tr '[a-z]' '[A-Z]'`
PROGRAM_MAIN_DIR="${0%/*}"
if [ $PROGRAM_MAIN_DIR == ${0} ]; then
	PROGRAM_MAIN_DIR=$(`echo ls -d $PWD/${0}`)
	PROGRAM_MAIN_DIR=${PROGRAM_MAIN_DIR%/*}
fi

SHELL_PATH="${PROGRAM_MAIN_DIR}/${0##*/}"

PROGRAM_DEFAULT_OUTPUT_DIR="${PROGRAM_MAIN_DIR}/output"
PROGRAM_DEFAULT_LOG_DIR="${PROGRAM_MAIN_DIR}/log"
PROGRAM_DEFAULT_BIN_DIR="${PROGRAM_MAIN_DIR}/bin"

JANDI_POST_ADDRESS="https://wh.jandi.com/connect-api/webhook/13041422/79c80cefb500c58d2978d02b38ee6f54"

PROGRAM_DEFAULT_STORAGE_NFS_PATH="DOWNLOAD_STORAGE:/storage"

PROGRAM_DEFAULT_DST_DIR="/patent/deutschland/normalized/dpma"

TARGET_COUNTRY_ENUM=`echo $1 | tr '[a-z]' '[A-Z]'`
if [ -z $TARGET_COUNTRY_ENUM ] || ! [[ $TARGET_COUNTRY_ENUM =~ ^(CN|DE|WO)$ ]]; then
    echo -e "ERROR\tILLEGAL_COUNTRY_ENUM($TARGET_COUNTRY_ENUM)"
    exit -1
fi

case $TARGET_COUNTRY_ENUM in
    'DE')
        PROGRAM_DEFAULT_DST_DIR="/patent/deutschland/normalized/dpma"
        LEXIS_DATA_TABLE="LEXIS_DATA_DPMA_PATENT_SOURCE"
		PROGRAM_SHELL_TO_EXECUTE="$PROGRAM_DEFAULT_BIN_DIR/relocator_for_DE.sh"
        ;;
    'CN')
        PROGRAM_DEFAULT_DST_DIR="/patent/deutschland/normalized/temp"
        LEXIS_DATA_TABLE="LEXIS_DATA_CN_PATENT_SOURCE"
		PROGRAM_SHELL_TO_EXECUTE="$PROGRAM_DEFAULT_BIN_DIR/relocator_for_DE.sh"
        ;;
    'WO')
        PROGRAM_DEFAULT_DST_DIR="/patent/deutschland/normalized/temp"
        # TODO: 아직 미구현
        exit -1
        ;;
    *)
        exit -1
        ;;
esac

INITIAL_FOUND_TARGET_WORKS=()
WORKING_QUEUE=()

LEXIS_FILE_TYPES=("XML" "IMAGES" "CLIP" "PDF")


function init_program() {
	if ! [ -d $PROGRAM_DEFAULT_LOG_DIR ]; then
		mkdir -p $PROGRAM_DEFAULT_LOG_DIR
	fi

	if ! [ -d $PROGRAM_DEFAULT_OUTPUT_DIR ]; then
		mkdir -p $PROGRAM_DEFAULT_OUTPUT_DIR
	fi
}


function notificate_to_JANDI() {
        local date=`date '+%Y-%m-%d %T'`
        local msg=$1
        local crit_level=$2

        local message_data=`echo {'"'body'"':'"'WORKER NODE RLEXISRT : ${PROGRAM_HOST_NAME}'"','"'connectColor'"':'"'\#FAC11B'"', '"'connectInfo'"': [{'"'title'"':'"' 내 용'"','"'description'"':'"'$msg'"'},{'"'title'"':'"' 일 시'"','"'description'"':'"'$date'"'}]}`
        curl -X POST ${JANDI_POST_ADDRESS} \
                -H "Accept: application/vnd.tosslab.jandi-v2+json" \
                -H "Content-Type: application/json" \
                --data-binary "${message_data}"
}


function end_of_process() {
	local ret_value=$1
	if [ -z $ret_value ]; then
		ret_value=0
	fi

    if [ -s ${SHELL_PATH}.pid ] ;then
		rm -f ${SHELL_PATH}.pid
	fi
	exit $ret_value
}


function check_dupl_executed() {
	if [ -s "${SHELL_PATH}.pid" ]; then
		exit
	fi
	echo $$ > "${SHELL_PATH}.pid"
}


function get_data() {
	local cond=$1
	local colm=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

    local query="select $colm from $table_name where $cond"
    local result=""
    IFS=$'\n' result=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
    if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi
	
	echo $result
}


function insert_data_to_db () {
	col_names=$1
	set_values=$2
	table_name=$3
	if [ -z $col_names ] || [ -z $set_values ]; then
		end_of_process -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	insert_query="INSERT INTO ${table_name} ($col_names) values (${set_values})";
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$insert_query";
	if [ $? != "0" ]; then
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi
}


function upsert_data_to_db () {
	local col_names=$1
	local set_values=$2
	local update_values=$3
	local table_name=$4
	if [ -z $col_names ] || [ -z $set_values ]; then
		end_of_process -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	local query="INSERT INTO ${table_name} ($col_names) VALUES (${set_values}) ON DUPLICATE KEY UPDATE $3;"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi
}

function update_data_to_db () {
	local conds=$1
	local set_values=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

	if [ ${#conds} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
			end_of_process -1
	fi

	local query="update ${table_name} set ${set_values}  where ${conds}"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -11
	fi
}


function extract_zip_file() {
	local src_path=$1
	local dst_dir=$2
	local output_dir=$3
	local src_file_name=${src_path##*/}

	if ! [ -f $src_path ]; then
		return 30
	fi

	if ! [ -d $dst_dir ]; then
		mkdir -p $dst_dir
	fi

	if ! [ -d $output_dir ]; then
		mkdir -p $output_dir
	elif [ -f $output_dir/${src_file_name}.err ]; then
		local err_size=`ls -al $output_dir/${src_file_name}.err | cut -d ' ' -f 5`
		if [ $err_size == "0" ]; then
			return 0
		fi
	fi
	unzip -o $src_path -d $dst_dir > $output_dir/${src_file_name}.out 2> $output_dir/${src_file_name}.err
	return $?
}

function validate_waiting_works() {
	local PUBL_DATE=$1
	local SRC_DIR=$2

	if ! [ -d $SRC_DIR ]; then
		return 1
	fi

	local cond="PUBL_DATE='$PUBL_DATE'"		
	local checkDir=0

	local NUM_XMLS=0
	local NUM_CLIPS=0
	local NUM_HAS_IMGS=0
	local NUM_IMGS=0
	local NUM_PDFS=0

	WORKING_QUEUE+=(`echo "$PUBL_DATE $SRC_DIR"`)
	return 0
	local i=0
	for ((i=0; i<${#LEXIS_FILE_TYPES[@]}; i++))
	do
		echo ${LEXIS_FILE_TYPES[i]}
		local src_dir=$SRC_DIR/${LEXIS_FILE_TYPES[i]}
		if ! [ -d $src_dir ]; then
			continue
		fi

		case ${LEXIS_FILE_TYPES[i]} in
			'XML')
				NUM_XMLS=`find $src_dir -type f -iname "*.xml" | wc -l`
			;;
			'CLIP')
				NUM_CLIPS=`find $src_dir -type f -iname "*.png" | wc -l`
			;;
			'IMAGES')
				NUM_HAS_IMGS=`find $src_dir -type f -iname "*.xml" | wc -l`
				NUM_IMGS=`find $src_dir -type f -iname "*.png" | wc -l`
			;;
			'PDF')
				NUM_PDFS=`find $src_dir -type f -iname "*.pdf" | wc -l`
			;;
		esac

	done
	WORKING_QUEUE+=(`echo "$PUBL_DATE $SRC_DIR"`)

#	local values="NUM_XMLS=$NUM_XMLS, NUM_CLIPS=$NUM_CLIPS, NUM_HAS_IMGS=$NUM_HAS_IMGS, NUM_IMGS=$NUM_IMGS, NUM_PDFS=$NUM_PDFS"
#	update_data_to_db "$cond" "$values"
	
	return 0
}


function check_waiting_works() {
	local query="select PUBL_DATE, SRC_FILE_DIR from $LEXIS_DATA_TABLE where EXTRACT_SIGNAL = 'SUCCESS' and NORMALIZE_SIGNAL is NULL;"
	local i=0
	IFS=$'\n' INITIAL_FOUND_TARGET_WORKS=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
	if [ ${#INITIAL_FOUND_TARGET_WORKS[@]} -gt 0 ]; then
		echo "* FOUND ${#INITIAL_FOUND_TARGET_WORKS[@]} files"
	else
		end_of_process 0
	fi
}


function main() { 
	if ! [ -d $PROGRAM_DEFAULT_BIN_DIR ] ;then
		exit -1
	fi
	
	if ! [ -f $PROGRAM_SHELL_TO_EXECUTE ]; then
		exit 1
	fi
	
	check_dupl_executed
	
	check_waiting_works
	if [ ${#INITIAL_FOUND_TARGET_WORKS[@]} -gt 0 ]; then
		local i=0;
		for ((i=0; i<${#INITIAL_FOUND_TARGET_WORKS[@]}; i++))
		do
			local idx=`expr $i + 1`
			local columns=""
			IFS=$'\t' columns=(${INITIAL_FOUND_TARGET_WORKS[i]})

			validate_waiting_works ${columns[0]} ${columns[1]}
			local result=$?
			IFS=$'\n'
			if [ $result -ne 0 ]; then
				echo -e "\t$idx\t(${columns[0]} ${columns[1]}) -> PASSED(NOT_PREPARED)"
				continue
			fi
			echo -e "\t$idx\t(${columns[0]} ${columns[1]}) -> ENQUEUED"

		done
	fi
	
	if [ ${#WORKING_QUEUE} -gt 0 ]; then
		echo -e "\n\nWORKING_QUEUE STARTED"
		local i=0;
		local max_works=${#WORKING_QUEUE[@]}
		for ((i=0; i<${#WORKING_QUEUE[@]}; i++))
		do
			local idx=`expr $i + 1`
			local columns=""
			echo -e "\t$idx\t(${columns[0]} ${columns[1]}) -> STARTED"
			IFS=$' ' columns=(${WORKING_QUEUE[i]})
			
			local pub_date=${columns[0]}
			local src_dir=${columns[1]}
			local dst_dir="$PROGRAM_DEFAULT_DST_DIR"
			local output_dir="$PROGRAM_DEFAULT_OUTPUT_DIR/${TARGET_COUNTRY_ENUM}/${pub_date:0:4}/${pub_date}"
			if ! [ -d $output_dir ]; then
				mkdir -p $output_dir
			fi

			local cond="PUBL_DATE='$pub_date'"		
			local values=""
			$PROGRAM_SHELL_TO_EXECUTE ${pub_date} $src_dir $dst_dir > $output_dir/${pub_date}.out 2>&1
#			echo "$PROGRAM_SHELL_TO_EXECUTE ${pub_date} $src_dir $dst_dir > $output_dir/${pub_date}.out 2>&1"
			if [ $? == 0 ]; then
        		local date=`date '+%Y-%m-%d %T'`
				values="NORMALIZE_SIGNAL='SUCCESS', NORMALIZE_END_DATE='$date', DST_FILE_DIR='$dst_dir'"
				echo -e "\t\t-> SUCCESS"
				echo
			else 
        		local date=`date '+%Y-%m-%d %T'`
				values="NORMALIZE_SIGNAL='FAILURE', NORMALIZE_END_DATE='$date', DST_FILE_DIR='$dst_dir'"
				echo -e "\t\t-> FAILURE"
			fi

#			echo $cond $values
			update_data_to_db "$cond" "$values"
		done
	fi
	
	end_of_process 0
}

main

