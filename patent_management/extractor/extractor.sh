#!/bin/bash

SHELL_PATH=${0}

#MYSQL_LOGIN_PATH="PATENT_MANAGEMENT_DB"
MYSQL_LOGIN_PATH="PMDB"
MYSQL_DATABASE_NAME="PATENT_MANAGEMENT_DB"
LEXIS_DATA_DOWNLOAD_MANAGER="LEXIS_DATA_DOWNLOAD_MANAGER"
MYSQL_DEFAULT_TABLE=${LEXIS_DATA_DOWNLOAD_MANAGER}


FTP_PROGRAM="lftp"
LEXIS_FTP_URL="sftp://ftpsg.univentio.com:22"
LEXIS_FTP_USER_INFO="WERT,kkfdoxQiQB6SzJuqXN6Z"

PROGRAM_HOST_NAME=`hostname | tr '[a-z]' '[A-Z]'`

PROGRAM_MAIN_DIR="${0%/*}"
if [ $PROGRAM_MAIN_DIR == ${0} ]; then
	PROGRAM_MAIN_DIR=$(`echo ls -d $PWD/${0}`)
	PROGRAM_MAIN_DIR=${PROGRAM_MAIN_DIR%/*}
fi

SHELL_PATH="${PROGRAM_MAIN_DIR}/${0##*/}"

PROGRAM_DEFAULT_OUTPUT_DIR="${PROGRAM_MAIN_DIR}/output"
PROGRAM_DEFAULT_LOG_DIR="${PROGRAM_MAIN_DIR}/log"

JANDI_POST_ADDRESS="https://wh.jandi.com/connect-api/webhook/13041422/79c80cefb500c58d2978d02b38ee6f54"

TARGET_COUNTRY_ENUM=`echo $1 | tr '[a-z]' '[A-Z]'`
if [ -z $TARGET_COUNTRY_ENUM ] || ! [[ $TARGET_COUNTRY_ENUM =~ ^(CN|DE|WO)$ ]]; then
	echo -e "ERROR\tILLEGAL_COUNTRY_ENUM($TARGET_COUNTRY_ENUM)"
	exit -1
fi

case $TARGET_COUNTRY_ENUM in 
	'DE')
		PROGRAM_DEFAULT_DST_DIR="/patent/deutschland/normalized/temp"
		LEXIS_DATA_TABLE="LEXIS_DATA_DPMA_PATENT_SOURCE"
		;;
	'CN')
		PROGRAM_DEFAULT_DST_DIR="/data/lexis/extract"
		LEXIS_DATA_TABLE="LEXIS_DATA_CN_PATENT_SOURCE"
		;;
	'WO')
		PROGRAM_DEFAULT_DST_DIR="/patent/temp"
		LEXIS_DATA_TABLE="LEXIS_DATA_WIPO_PATENT_SOURCE"
		;;
	*) 
		exit -1
		;;
esac


INITIAL_FOUND_TARGET_WORKS=()
WORKING_QUEUE=()


function init_program() {
	if ! [ -d $PROGRAM_DEFAULT_LOG_DIR ]; then
		mkdir -p $PROGRAM_DEFAULT_LOG_DIR
	fi

	if ! [ -d $PROGRAM_DEFAULT_OUTPUT_DIR ]; then
		mkdir -p $PROGRAM_DEFAULT_OUTPUT_DIR
	fi
}

function notificate_to_JANDI() {
        local date=`date '+%Y-%m-%d %T'`
        local msg=$1
        local crit_level=$2

        local message_data=`echo {'"'body'"':'"'HOST_NAME : ${PROGRAM_HOST_NAME}'"','"'connectColor'"':'"'\#FAC11B'"', '"'connectInfo'"': [{'"'title'"':'"' 내 용'"','"'description'"':'"'$msg'"'},{'"'title'"':'"' 일 시'"','"'description'"':'"'$date'"'}]}`
        curl -X POST ${JANDI_POST_ADDRESS} \
                -H "Accept: application/vnd.tosslab.jandi-v2+json" \
                -H "Content-Type: application/json" \
                --data-binary "${message_data}"
}

function end_of_process() {
	local ret_value=$1
	if [ -z $ret_value ]; then
		ret_value=0
	fi

    if [ -s ${SHELL_PATH}.pid ] ;then
		rm -f ${SHELL_PATH}.pid
	fi
	exit $ret_value
}


function check_dupl_executed() {
	if [ -s "${SHELL_PATH}.pid" ]; then
		exit
	fi
	echo $$ > "${SHELL_PATH}.pid"
}


function get_data() {
	local cond=$1
	local colm=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

    local query="select $colm from $table_name where $cond"
    local result=""
    IFS=$'\n' result=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
    if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi
	
	echo $result
}


function insert_data_to_db () {
	col_names=$1
	set_values=$2
	table_name=$3
	if [ -z $col_names ] || [ -z $set_values ]; then
		end_of_process -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	insert_query="INSERT INTO ${table_name} ($col_names) values (${set_values})";
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$insert_query";
	if [ $? != "0" ]; then
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi
}


function upsert_data_to_db () {
	local col_names=$1
	local set_values=$2
	local update_values=$3
	local table_name=$4
	if [ -z $col_names ] || [ -z $set_values ]; then
		end_of_process -1
	elif [ -z $table_name ]; then
		table_name="$MYSQL_DEFAULT_TABLE"
	fi

	local query="INSERT INTO ${table_name} ($col_names) VALUES (${set_values}) ON DUPLICATE KEY UPDATE $3;"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi
}

function update_data_to_db () {
	local conds=$1
	local set_values=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

	if [ ${#conds} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
			end_of_process -1
	fi

	local query="update ${table_name} set ${set_values}  where ${conds}"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -11
	fi
}



function extract_zip_file() {
	local src_path=$1
	local dst_dir=$2
	local output_dir=$3
	local src_file_name=${src_path##*/}

	if ! [ -f $src_path ]; then
		return 30
	fi

	if ! [ -d $dst_dir ]; then
		mkdir -p $dst_dir
	fi

	if ! [ -d $output_dir ]; then
		mkdir -p $output_dir
	#elif [ -f $output_dir/${src_file_name}.err ]; then
	#	local err_size=`ls -al $output_dir/${src_file_name}.err | cut -d ' ' -f 5`
	#	if [ $err_size == "0" ]; then
	#		return 0
	#	fi
	fi
	unzip -o $src_path -d $dst_dir > $output_dir/${src_file_name}.out 2> $output_dir/${src_file_name}.err
	return $?
}

function extract_zip_files() {
	local BATCH_TYPE=$1
	local REQUEST_START_DATE=$2
#	local query="select FILE_TYPE, DOWNLOADED_FILE_PATH, BATCH_ID, FILE_NAME from PATENT_MANAGEMENT_DB.LEXIS_DATA_DOWNLOAD_MANAGER where BATCH_TYPE='$BATCH_TYPE' and REQUEST_START_DATE='$REQUEST_START_DATE' and DOWNLOADED_STATUS = 'SUCCESS' and EXTRACT_FILE is NULL and COUNTRY_ENUM = '$TARGET_COUNTRY_ENUM';"
	local query="select FILE_TYPE, DOWNLOADED_FILE_PATH, BATCH_ID, FILE_NAME from PATENT_MANAGEMENT_DB.LEXIS_DATA_DOWNLOAD_MANAGER where BATCH_TYPE='$BATCH_TYPE' and REQUEST_START_DATE='$REQUEST_START_DATE' and DOWNLOADED_STATUS = 'SUCCESS' and COUNTRY_ENUM = '$TARGET_COUNTRY_ENUM' and (EXTRACT_FILE is NULL OR EXTRACT_FILE != 'SUCCESS') order by REQUEST_START_DATE asc;"
	local work_list=""
	local i=0

	local num_success=0
	local num_failure=0


	IFS=$'\n' work_list=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
	for ((i=0; i<${#work_list[@]}; i++))
	do
		local idx=`expr $i + 1`
		local columns=""
		IFS=$'\t' columns=(${work_list[i]})
		
		
		local FILE_TYPE="${columns[0]}"
		local DOWNLOADED_FILE_PATH="${columns[1]}"
		local cond="BATCH_ID='${columns[2]}' and FILE_NAME='${columns[3]}'"
		local dst_dir=${PROGRAM_DEFAULT_DST_DIR}/${TARGET_COUNTRY_ENUM}/${BATCH_TYPE}/${REQUEST_START_DATE}/${FILE_TYPE}
		local output_dir=${PROGRAM_DEFAULT_OUTPUT_DIR}/${TARGET_COUNTRY_ENUM}/${BATCH_TYPE}/${REQUEST_START_DATE}/${FILE_TYPE}

		echo -e "\t($idx/${#work_list[@]}) WORK_START($FILE_TYPE): $DOWNLOADED_FILE_PATH"
		extract_zip_file $DOWNLOADED_FILE_PATH $dst_dir $output_dir
		if [ $? == "0" ]; then
			echo -e "\t\t-> SUCCESS($dst_dir)"
			num_success=`expr $num_success + 1`
			update_data_to_db "$cond" "EXTRACT_FILE='SUCCESS'"
		else
			num_failure=`expr $num_failure + 1`
			if [ $? == 30 ]; then
				echo -e "\t\t-> FILE_NOT_FOUND"
				update_data_to_db "$cond" "EXTRACT_FILE='FILE_NOT_FOUND'"
			else 
				echo -e "\t\t-> EXTRACT_FAILURE"
				update_data_to_db "$cond" "EXTRACT_FILE='ERROR'"
			fi
		fi
	done
	local date=`date '+%Y-%m-%d %T'`

	if [ $num_success -eq ${#work_list[@]} ]; then
		local dst_dir="${PROGRAM_DEFAULT_DST_DIR}/${TARGET_COUNTRY_ENUM}/${BATCH_TYPE}/${REQUEST_START_DATE}"
		local extracted_count=$(get_data "BATCH_TYPE='$BATCH_TYPE' and REQUEST_START_DATE='$REQUEST_START_DATE' AND COUNTRY_ENUM='$TARGET_COUNTRY_ENUM' and EXTRACT_FILE='SUCCESS'" "COUNT(*)" "PATENT_MANAGEMENT_DB.LEXIS_DATA_DOWNLOAD_MANAGER")
		local total_count=$(get_data "BATCH_TYPE='$BATCH_TYPE' and REQUEST_START_DATE='$REQUEST_START_DATE' AND COUNTRY_ENUM='$TARGET_COUNTRY_ENUM'" "SUM(DISTINCT BATCH_GROUP_SIZE)" "PATENT_MANAGEMENT_DB.LEXIS_DATA_DOWNLOAD_MANAGER")
		if ! [ -z $total_count ] && ! [ -z $extracted_count ] && [ $total_count == $extracted_count ]; then
			upsert_data_to_db "WORKER_ID, PUBL_DATE, SRC_FILE_DIR, EXTRACT_SIGNAL, REGISTER_DATE" "'$PROGRAM_HOST_NAME', '$REQUEST_START_DATE', '$dst_dir', 'SUCCESS', '$date'" "WORKER_ID=VALUES(WORKER_ID), SRC_FILE_DIR=VALUES(SRC_FILE_DIR), EXTRACT_SIGNAL=VALUES(EXTRACT_SIGNAL), SRC_FILE_DIR=VALUES(SRC_FILE_DIR), REGISTER_DATE=VALUES(REGISTER_DATE), EXTRACT_SIGNAL=VALUES(EXTRACT_SIGNAL)" "$LEXIS_DATA_TABLE"
		else
			upsert_data_to_db "WORKER_ID, PUBL_DATE, SRC_FILE_DIR, EXTRACT_SIGNAL, REGISTER_DATE" "'$PROGRAM_HOST_NAME', '$REQUEST_START_DATE', '$dst_dir', 'REMAIN_OTHER_FILES', '$date'" "WORKER_ID=VALUES(WORKER_ID), SRC_FILE_DIR=VALUES(SRC_FILE_DIR), EXTRACT_SIGNAL=VALUES(EXTRACT_SIGNAL), SRC_FILE_DIR=VALUES(SRC_FILE_DIR), REGISTER_DATE=VALUES(REGISTER_DATE), EXTRACT_SIGNAL=VALUES(EXTRACT_SIGNAL)" "$LEXIS_DATA_TABLE"
		fi
		return 0
	else 
		local dst_dir="${PROGRAM_DEFAULT_DST_DIR}/${BATCH_TYPE}/${REQUEST_START_DATE}"
		upsert_data_to_db "WORKER_ID, PUBL_DATE, SRC_FILE_DIR, EXTRACT_SIGNAL, REGISTER_DATE" "'$PROGRAM_HOST_NAME', '$REQUEST_START_DATE', '$dst_dir', 'FAILURE', '$date'" "WORKER_ID=VALUES(WORKER_ID), SRC_FILE_DIR=VALUES(SRC_FILE_DIR), EXTRACT_SIGNAL=VALUES(EXTRACT_SIGNAL), SRC_FILE_DIR=VALUES(SRC_FILE_DIR), REGISTER_DATE=VALUES(REGISTER_DATE)" "$LEXIS_DATA_TABLE"
		return 1
	fi
}


function validate_waiting_works() {
	local BATCH_TYPE=$1
	local REQUEST_START_DATE=$2
	
	local query="select distinct BATCH_GROUP_SIZE, BATCH_TYPE, REQUEST_START_DATE, FILE_TYPE from PATENT_MANAGEMENT_DB.LEXIS_DATA_DOWNLOAD_MANAGER where BATCH_TYPE='$BATCH_TYPE' and REQUEST_START_DATE='$REQUEST_START_DATE' and DOWNLOADED_STATUS = 'SUCCESS' and COUNTRY_ENUM = '$TARGET_COUNTRY_ENUM' and (EXTRACT_FILE is NULL OR EXTRACT_FILE != 'SUCCESS');"
	local work_list=""
	local i=0

	local retValue=0

	IFS=$'\n' work_list=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
	if [ ${#work_list[@]} -eq 3 ]; then		# PDF/XML/IMAGE 3종세트...인가?
		for ((i=0; i<${#work_list[@]}; i++))
		do
			local columns=""
			IFS=$'\t' columns=(${work_list[i]})

			local cond="COUNTRY_ENUM='$TARGET_COUNTRY_ENUM' and BATCH_TYPE='${columns[1]}' and REQUEST_START_DATE='${columns[2]}' and FILE_TYPE='${columns[3]}' and DOWNLOADED_STATUS='SUCCESS'"
			local cols="COUNT(*)"
#			echo $cond $cols
			local retSize=$(get_data $cond $cols)
#			echo $retSize ${columns[0]}
			if [ $retSize != ${columns[0]} ]; then
				retValue=1
			fi
			IFS=$'\n'
		done
	fi

#	if [ $retValue -eq 0 ]; then
		WORKING_QUEUE+=(`echo -e "$BATCH_TYPE\t$REQUEST_START_DATE"`)
#	fi

	return $retValue
}


function check_waiting_works() {
	local query="select distinct BATCH_TYPE, REQUEST_START_DATE from PATENT_MANAGEMENT_DB.LEXIS_DATA_DOWNLOAD_MANAGER where BATCH_TYPE = 'UPDATED' and DOWNLOADED_STATUS = 'SUCCESS' and (EXTRACT_FILE is NULL OR EXTRACT_FILE != 'SUCCESS')  and COUNTRY_ENUM = '$TARGET_COUNTRY_ENUM';"
	local i=0

	IFS=$'\n' INITIAL_FOUND_TARGET_WORKS=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
	if [ ${#INITIAL_FOUND_TARGET_WORKS[@]} -gt 0 ]; then
		echo "* FOUND ${#INITIAL_FOUND_TARGET_WORKS[@]} files"
	else
		echo -e "NOTICE\tTHERE ARE NO WORKS"
		end_of_process 0
	fi
}


function main() { 
	check_dupl_executed

	check_waiting_works
	if [ ${#INITIAL_FOUND_TARGET_WORKS[@]} -gt 0 ]; then
		local i=0;
		for ((i=0; i<${#INITIAL_FOUND_TARGET_WORKS[@]}; i++))
		do
			echo ${INITIAL_FOUND_TARGET_WORKS[i]}
			local idx=`expr $i + 1`
			local columns=""
			IFS=$'\t' columns=(${INITIAL_FOUND_TARGET_WORKS[i]})
			validate_waiting_works ${columns[0]} ${columns[1]}
			local result=$?
			IFS=$'\n'
#			if [ $result -ne 0 ]; then
#				echo -e "\t$idx\t(${columns[0]} ${columns[1]}) -> PASSED(NOT_PREPARED)"
#				continue
#			fi
			echo -e "\t$idx\t(${columns[0]} ${columns[1]}) -> ENQUEUED"

		done
	fi

	if [ ${#WORKING_QUEUE[@]} -gt 0 ]; then
		echo -e "\n\nWORKING_QUEUE STARTED\n\n"
		local i=0;
		local max_works=${#WORKING_QUEUE[@]}
		echo $max_works
		for ((i=0; i<${#WORKING_QUEUE[@]}; i++))
		do
			local idx=`expr $i + 1`
			local columns=""
			IFS=$'\t' columns=(${WORKING_QUEUE[i]})
			extract_zip_files ${columns[0]} ${columns[1]}
			if [ $? != "0" ]; then
				echo -e "\tERROR OCCURED: ${columns[0]} ${columns[1]}"
				continue
			fi
		done
		
		echo -e "\n\nWORKING_QUEUE FINISHED\n\n"
	fi
	
	end_of_process 0
}

main

