#!/bin/bash

MYSQL_LOGIN_PATH="PMDB"
MYSQL_DATABASE_NAME="PATENT_MANAGEMENT_DB"
LEXIS_DATA_TABLE=""

PROGRAM_HOST_NAME=`hostname | tr '[a-z]' '[A-Z]'`
PROGRAM_MAIN_DIR="${0%/*}"
if [ $PROGRAM_MAIN_DIR == ${0} ]; then
	PROGRAM_MAIN_DIR=$(`echo ls -d $PWD/${0}`)
	PROGRAM_MAIN_DIR=${PROGRAM_MAIN_DIR%/*}
fi

SHELL_PATH="${PROGRAM_MAIN_DIR}/${0##*/}"

TARGET_PUBLICATION_DATE=""

case $1 in
	'WO')
		COUNTRY_ENUM="WO"
		FILE_SERVER_DIRS=('wipo_1' 'wipo_2')
		LEXIS_DATA_TABLE="WIPO_SOURCE_FILE_MANAGEMENT"	;;
	'CN')
		exit -1
		COUNTRY_ENUM="CN"
		LEXIS_DATA_TABLE="SIPO_SOURCE_FILE_MANAGEMENT"	;;
	'DE')
		exit -1
		COUNTRY_ENUM="DE"
		FILE_SERVER_DIRS=('dpma')
		LEXIS_DATA_TABLE="DPMA_SOURCE_FILE_MANAGEMENT"	;;
	*)
		echo "USAGE: ${SHELL_PATH} COUNTRY_ENUM(WO|CN|DE) DATE(20190101)"
		exit -1
esac

if [ -z $2 ] || ! [[ $2 =~ ^(19|20)[0-9]{2}[0-1][0-9][0-3][0-9]$ ]]; then
	echo "USAGE: ${SHELL_PATH} COUNTRY_ENUM(WO|CN|DE) DATE(20190101)"
	exit -1
fi 
TARGET_PUBLICATION_DATE=$2

function end_of_process() {
	local ret_value=$1
	if [ -z $ret_value ]; then
		ret_value=0
	fi

    if [ -s ${SHELL_PATH}.pid ] ;then
		rm -f ${SHELL_PATH}.pid
	fi
	exit $ret_value
}


function check_dupl_executed() {
	if [ -s "${SHELL_PATH}.pid" ]; then
		exit
	fi
	echo $$ > "${SHELL_PATH}.pid"
}


function get_data() {
	local cond=$1
	local colm=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

    local query="select $colm from $table_name where $cond"
    local result=""
    IFS=$'\n' result=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)
    if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		end_of_process -1
	fi

	echo $result
}


function update_data_to_db () {
	local conds=$1
	local set_values=$2
	local table_name=$3
	if [ -z $table_name ]; then
		table_name=${MYSQL_DEFAULT_TABLE}
	fi

	if [ ${#conds} -eq 0 ] || [ ${#set_values} -eq 0 ]; then
			end_of_process -1
	fi

	local query="update ${table_name} set ${set_values}  where ${conds}"
	mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -e "$query";
	if [ $? != "0" ]; then
		echo $query
		echo -e "ERROR:\tCheck your DB connection"
		exit -11
	fi
}


function convert_png_to_resized_img() {
	local src_file_path=$1
	local src_file_name=${src_file_path##*/}
	local dst_dir=${src_file_path%/*.*}
	local postfix
	if ! [ -f $src_file_path ]; then
		return 0
	fi

	result=0
	for postfix in `echo -e '600\n360\n180'`
	do
		local option
		local dst_file_name
		case $postfix in
			'180')
		        option="180x180"
				dst_file_name="${src_file_name%.*}_s.jpg"
				;;
			'360')
		        option="360x360"
				dst_file_name="${src_file_name%.*}_m.jpg"
				;;
			'600')
		        option="600x600"
				dst_file_name="${src_file_name%.*}_l.jpg"
				;;
		esac

		if [ -f $dst_dir/$dst_file_name ]; then
			result=`expr $result + 1`
			continue
		fi

#		echo "convert $option $src_file_path $dst_dir/$dst_file_name"
		convert -density 300 -units PixelsPerInch -resize "$option" "$src_file_path" "$dst_dir/$dst_file_name"
		if [ $? == "0" ]; then
			result=`expr $result + 1`
		fi
	done

	return $result
}


function transfer_file_to_fs() {
	local file_path=$1
	local dst_dir=$2
	local dst_file_name=${file_path##*/}
	local idx
#	echo -e "$file_path\t$dst_dir\t$dst_file_name"

	local result=0

#	echo ${#FILE_SERVER_DIRS[@]}	
	for ((idx=0; idx<${#FILE_SERVER_DIRS[@]}; idx++))
	do

		local fileserver_dir="/fileservers/${FILE_SERVER_DIRS[idx]}/$dst_dir"
		if ! [ -d $fileserver_dir ]; then
			mkdir -p $fileserver_dir
		fi
		
		if ! [ -f $fileserver_dir/$dst_file_name ]; then
#			echo "cp $file_path $fileserver_dir/$dst_file_name"
			cp $file_path $fileserver_dir/$dst_file_name
			if [ $? != "0" ]; then
				result=`expr $result + 1`
			fi
		fi
	done

	if [ $result -eq 0 ]; then
		return 0
	fi

	return 1

#FILE_SERVER_DIRS=('wipo_1', 'wipo_2')
}

function transfer_pdf_to_fs() {
	local document_id=$1
	local src_dir=$2

	local relative_dir
	case $COUNTRY_ENUM in
		'WO') 
			relative_dir="${document_id:2:4}/${document_id:7:3}/${document_id:2:4}${document_id:7:6}/${document_id:13}/pdf"
			;;
		'CN')
			relative_dir=${src_dir#/sipo}
			;;
		'DE')
			relative_dir="${document_id:2:2}/${document_id:4:4}/${document_id:8:3}/${document_id:2:12}/${document_id:14}/pdf"
			;;
	esac

	local result=0
	for pdf_path in `find $src_dir -type f | egrep -i "\.(pdf)$"`
	do
#		echo $pdf_path $relative_dir
		transfer_file_to_fs $pdf_path $relative_dir
		if [ $? != "0" ]; then
			result=`expr $result + 1`
		fi
	done
	

	if [ $? -eq 0 ]; then
		update_data_to_db "DOCUMENT_ID='$document_id'" "FS_PDF_STATUS='SUCCESS'" "$LEXIS_DATA_TABLE"
		return 0
	fi
	
	update_data_to_db "DOCUMENT_ID='$document_id'" "FS_PDF_STATUS='FAILURE'" "$LEXIS_DATA_TABLE"
	return 1
}

function transfer_image_to_fs() {
	local document_id=$1
	local src_dir=$2

	local relative_dir
	case $COUNTRY_ENUM in
		'WO') 
			relative_dir="${document_id:2:4}/${document_id:7:3}/${document_id:2:4}${document_id:7:6}/${document_id:13}/img"
			;;
		'CN')
			relative_dir=${src_dir#/sipo}
			;;
		'DE')
			relative_dir="${document_id:2:2}/${document_id:4:4}/${document_id:8:3}/${document_id:2:12}/${document_id:14}/img"
			;;
	esac

	local result=0
	for img_path in `find $src_dir -type f | egrep -i "\.(jpg|png)$"`
	do
#		echo $img_path $relative_dir
		transfer_file_to_fs $img_path $relative_dir
		if [ $? != "0" ]; then
			result=`expr $result + 1`
		fi
	done
	

	if [ $? -eq 0 ]; then
		update_data_to_db "DOCUMENT_ID='$document_id'" "FS_IMAGE_STATUS='SUCCESS'" "$LEXIS_DATA_TABLE"
		return 0
	fi
	
	update_data_to_db "DOCUMENT_ID='$document_id'" "FS_IMAGE_STATUS='FAILURE'" "$LEXIS_DATA_TABLE"
	return 1
}


function check_and_deploy() {
	local data_set="$1"
	local seqNumber=$2
	local max_works=$3

	IFS=$'\t' buffer=(`echo -e "${data_set}"`)

	if [ ${#buffer[@]} -ne 4 ] ;then
		IFS=$'\n' 
		return -1
	fi

	local document_id=${buffer[0]}
	local src_dir=${buffer[1]}
	local img_name=${buffer[2]}
	local pdf_name=${buffer[3]}

	IFS=$'\n'
	if ! [ -d $src_dir ]; then
		return 1
	fi

	echo -e "($seqNumber/$max_works)\t$document_id"
	if [ $img_name != "NULL" ]; then
		local img_path
		if [ $COUNTRY_ENUM === "WO" ]; then
			for img_path in `find $src_dir -type f -iname "*.png" | sort`
			do
				convert_png_to_resized_img "$src_dir/$img_name"
			done
		fi
		
		transfer_image_to_fs $document_id $src_dir
	fi
	
	if ! [ -z $pdf_name ] && [ $pdf_name != "NULL" ]; then
		transfer_pdf_to_fs $document_id $src_dir
	fi
	IFS=$'\n' 
}

function main() {
	check_dupl_executed

	local query="SELECT DOCUMENT_ID, STORAGE_DIR, ORIGIN_IMAGE_FILES, PDF_FILE_NAME FROM $LEXIS_DATA_TABLE WHERE PUBLICATION_DATE='$TARGET_PUBLICATION_DATE';" 
	local retrievedData
	IFS=$'\n' retrievedData=(`mysql --login-path=$MYSQL_LOGIN_PATH $MYSQL_DATABASE_NAME -se "$query";`)

	if ! [ -z $retrievedData ] && [ ${#retrievedData[@]} -gt 0 ]; then
		echo -e "\tFOUND : ${#retrievedData[@]}"
		
		local i
		for ((i=0; i<${#retrievedData[@]}; i++))
		do
			check_and_deploy "${retrievedData[i]}" $i ${#retrievedData[@]}
			IFS=$'\n'
		done

		echo -e "\n\nFINISHED"
	else
		echo -e "\tThere are no works\n\nFINISHED"
	fi

	end_of_process 0
}



main
